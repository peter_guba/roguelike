package cz.cuni.gamedev.nail123.roguelike.world.worlds

import com.jhlabs.vecmath.Tuple4f
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.*
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.world.Area
import cz.cuni.gamedev.nail123.roguelike.world.World
import cz.cuni.gamedev.nail123.roguelike.world.builders.EmptyAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D
import cz.cuni.gamedev.nail123.roguelike.blocks.*
import cz.cuni.gamedev.nail123.roguelike.entities.objects.*
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.*
import cz.cuni.gamedev.nail123.roguelike.entities.items.*
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import kotlin.random.Random
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles.WALL
import kotlin.math.min
import kotlin.math.sqrt

/**
 * Sample world, made as a starting point for creating custom worlds.
 * It consists of separate levels - each one has one staircase, and it leads infinitely deep.
 */
class SampleWorld: World() {
    var currentLevel = 0
    lateinit var blocks: Array<Array<Boolean>>

    override fun buildStartingArea() = buildLevel()

    /**
     * Builds one of the levels.
     */
    fun buildLevel(): Area {
        // Start with an empty area
        val areaBuilder = EmptyAreaBuilder().create()

        val count = 15
        // A substitute array for the actual array of blocks. Easier to work with as it's just an
        // array of booleans. If a spot is marked as true, that means there is a wall there.
        blocks = Array(areaBuilder.width) { Array(areaBuilder.height) { false } }

        // Add walls along the upper and lower borders.
        for (i in blocks.indices) {
            blocks[i][0] = true
            blocks[i][blocks[0].size - 1] = true
        }

        // Add walls along the left and right borders.
        for (i in blocks[0].indices) {
            blocks[0][i] = true
            blocks[blocks.size - 1][i] = true
        }

        // Subdivide the map a couple of times.
        for (i in 0 until count) {
            divide(i % 2 == 0, areaBuilder)
        }

        // A secondary substitute array. Not sure why but things didn't work properly when I just added
        // the walls straight to the area builder in this for loop.
        val blocks2 =  Array(areaBuilder.width) { Array(areaBuilder.height) { false } }

        // "Inflate" the walls to take up 5 positions instead of 1 - this will create spaces between the rooms.
        val boundary = 2
        for (i in blocks.indices) {
            for (j in blocks[0].indices) {
                if (blocks[i][j]) {
                    for (k in -boundary..boundary) {
                        if (i + k >= 0 && i + k < blocks.size) {
                            for (l in -boundary..boundary) {
                                if (j + l >= 0 && j + l < blocks[0].size) {
                                    blocks2[i + k][j + l] = true
                                }
                            }
                        }
                    }
                }
            }
        }

        // Transfer the walls to the areaBuilder but only if they aren't surrounded by walls on all sides.
        // (This gets rid of the excess walls created in the previous step and creates free space for the
        // player to walk through.)
        for (i in blocks2.indices) {
            for (j in blocks2[0].indices) {
                if (blocks2[i][j] && !surrounded(i, j, blocks2)) {
                    areaBuilder.blocks[Position3D.create(i, j, 0)] = Wall()
                }
                else {
                    areaBuilder.blocks[Position3D.create(i, j, 0)] = Floor()
                }
            }
        }

        val rooms = mutableListOf<Tuple4f>()

        // Get the x and y coordinates of all the rooms as well as their height and width.
        // (The x and y coordinates correspond to the coordinates of the empty spot in the
        // upper left corner of the room).
        for (i in 0 until blocks2.size - 1) {
            for (j in 0 until blocks2[0].size - 1) {
                if (!blocks2[i][j] && (i == 0 || blocks2[i - 1][j]) && (j == 0 || blocks2[i][j - 1])) {
                    var width = 0

                    while (i + width + 1 < blocks2.size && !blocks2[i + width + 1][j]) {
                        width++
                    }

                    var height = 0

                    while (j + height + 1 < blocks2[0].size && !blocks2[i][j + height + 1]) {
                        height++
                    }

                    rooms.add(Tuple4f(i.toFloat(), j.toFloat(), width.toFloat(), height.toFloat()))
                }
            }
        }

        // 3 Tiers of enemies, each with max 10 levels and max ten top-tier enemies per room.
        val maxDifficulty = 300
        val maxReward = 100

        val stairsIndex = Random.nextInt(0, rooms.size)

        // Add a door to the middle of a random wall of each room. Also decide the room's difficulty
        // and reward parameters and generate enemies and loot based on that and
        // place the stairs in one of the rooms.
        for (r in rooms.indices) {
            var wall = Random.nextInt(0, 4)
            var x = -1
            var y = -1

            // For the given wall, check whether it is possible to place a door there
            // (i.e. if it isn't next to the border of the world).
            while (x == -1 && y == -1) {
                if (wall == 0) {
                    x = (rooms[r].x + rooms[r].z / 2).toInt()
                    y = rooms[r].y.toInt() - 1

                    if (y == 0 || y == -1) {
                        wall++
                        x = -1
                        y = -1
                    }
                } else if (wall == 1) {
                    x = (rooms[r].x + rooms[r].z).toInt() + 1
                    y = (rooms[r].y + rooms[r].w / 2).toInt()

                    if (x == blocks2.size || x == blocks.size - 1) {
                        wall++
                        x = -1
                        y = -1
                    }
                } else if (wall == 2) {
                    x = (rooms[r].x + rooms[r].z / 2).toInt()
                    y = (rooms[r].y + rooms[r].w).toInt() + 1

                    if (y == blocks2[0].size || y == blocks2[0].size - 1) {
                        wall++
                        x = -1
                        y = -1
                    }
                } else {
                    x = rooms[r].x.toInt() - 1
                    y = (rooms[r].y + rooms[r].w / 2).toInt()

                    if (x == 0 || x == -1) {
                        wall = 0
                        x = -1
                        y = -1
                    }
                }
            }

            val position = Position3D.create(x, y, 0)
            areaBuilder.blocks[position] = GameBlock(GameTiles.EMPTY)
            areaBuilder.addEntity(Door(), position)

            val roomCapacity = (rooms[r].z * rooms[r].w / 4).toInt() * 3
            val difficulty = Random.nextInt(0, min(min((currentLevel + 1) * 10, maxDifficulty), roomCapacity) + 1)
            val reward = Random.nextInt(0, min(min((currentLevel + 1) * 10, maxReward), roomCapacity) + 1)

            // Add enemies to the room.
            var counter = difficulty
            while (counter != 0) {
                counter -= addEnemyToRoom(rooms[r], counter, areaBuilder)
            }

            // Add useful items to the room.
            counter = reward
            while (counter != 0) {
                counter -= addItemToRoom(rooms[r], counter, areaBuilder)
            }

            // Place the staircase in a randomly selected room.
            if (stairsIndex == r) {
                val stairsX = Random.nextInt((rooms[r].x).toInt(), (rooms[r].x + rooms[r].z).toInt() + 1)
                val stairsY = Random.nextInt((rooms[r].y).toInt(), (rooms[r].y + rooms[r].w).toInt() + 1)
                areaBuilder.addEntity(Stairs(), Position3D.create(stairsX, stairsY, 0))
            }
        }

        // Add the player at position (0, 0).
        areaBuilder.addEntity(areaBuilder.player, Position3D.create(0, 0, 0))

        // We add fog of war such that exploration is needed
        areaBuilder.addEntity(FogOfWar(), Position3D.unknown())

        // Build it into a full Area
        return areaBuilder.build()
    }

    /**
     * Moving down - goes to a brand new level.
     */
    override fun moveDown() {
        ++currentLevel
        this.logMessage("Descended to level ${currentLevel + 1}")
        if (currentLevel >= areas.size) areas.add(buildLevel())
        goToArea(areas[currentLevel])
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    override fun moveUp() {
        // Not implemented
    }

    // An implementation of one step of the binary division algorithm - adds a line of wall blocks
    // to the map, separating one of the created rooms into two.
    // The b parameter decides whether the split should be done in the x or y direction.
    // True <=> x
    fun divide(b: Boolean, builder: AreaBuilder) {
        var upperBound1 = 0
        var upperBound2 = 0

        if (b) {
            upperBound1 = builder.width - 1
            upperBound2 = builder.height - 1
        }
        else {
            upperBound1 = builder.height - 1
            upperBound2 = builder.width - 1
        }

        // Pick a random coordinate along which the split should be created.
        val coord = Random.nextInt(0, upperBound1)
        var nextBlockFree = true

        // Get the other coordinate of all the walls along the first selected coordinate.
        val walls = mutableListOf<Int>()
        walls.add(1)

        for (i in 0..upperBound2 - 2) {
            if (b) {
                nextBlockFree = !blocks[coord][i + 1]
            }
            else {
                nextBlockFree = !blocks[i + 1][coord]
            }

            if (!nextBlockFree) {
                walls.add(i + 1)
            }
        }

        // Pick a starting spot from the previously computed array of wall coordinates.
        var counter = walls[Random.nextInt(0, walls.size)]

        while(counter < upperBound2 && nextBlockFree) {
            if (b) {
                blocks[coord][counter] = true
                nextBlockFree = !blocks[coord][counter + 1]
            }
            else {
                blocks[counter][coord] = true
                nextBlockFree = !blocks[counter + 1][coord]
            }

            counter++
        }
    }

    // Checks whether a block is surrounded by wall blocks.
    fun surrounded(i: Int, j: Int, blocks: Array<Array<Boolean>>): Boolean {
        var res = true

        for (a in -1..1) {
            for (b in -1..1) {
                if (i + a >= 0 && i + a < blocks.size && j + b >= 0 && j + b < blocks[0].size) {
                    res = res && blocks[i + a][j + b]
                }
            }
        }

        return res
    }

    // Adds a random enemy to a given room and assigns it its stats based on the allocated budget.
    fun addEnemyToRoom(room: Tuple4f, budget:Int, builder: AreaBuilder): Int {
        val r = min(10, Random.nextInt(1, budget + 1))
        val x = Random.nextInt(room.x.toInt(), (room.x + room.z).toInt() + 1)
        val y = Random.nextInt(room.y.toInt(), (room.y + room.w).toInt() + 1)

        var expandedBudget = 4 * r - 4

        var smellingRadius = 1 + Random.nextInt(0, expandedBudget + 1)
        smellingRadius = min(smellingRadius, 10)
        expandedBudget = expandedBudget - smellingRadius + 1
        var hp = 1 + Random.nextInt(0, expandedBudget + 1)
        hp = min(hp, 10)
        expandedBudget = expandedBudget - hp + 1
        var attack = 1 + Random.nextInt(0, expandedBudget + 1)
        attack = min(attack, 10)
        expandedBudget = expandedBudget - attack + 1
        var defense = 1 + expandedBudget
        defense = min(defense, 10)

        var tier = Random.nextInt(1, 4)
        tier = min(tier, budget / r)

        var enemy: Enemy
        if (tier == 1) {
            enemy = Rat(tier * smellingRadius, tier * hp, tier * attack, tier * defense, r)
        }
        else if (tier == 2) {
            enemy = Orc(tier * smellingRadius, tier * hp, tier * attack, tier * defense, r)
        }
        else {
            enemy = Golem(tier * smellingRadius, tier * hp, tier * attack, tier * defense, r)
        }

        builder.addEntity(enemy, Position3D.create(x, y, 0))

        return tier * r
    }

    // Adds a random item to a given room and assigns it its stat based on the allocated budget.
    fun addItemToRoom(room: Tuple4f, budget:Int, builder: AreaBuilder): Int {
        val r1 = Random.nextInt(0, 3)
        val r2 = min(20, Random.nextInt(1, budget + 1))

        var item: Item
        if (r1 == 0) {
            item = Sword(r2)
        }
        else if (r1 == 1) {
            item = MetalArmor(r2)
        }
        else {
            item = Potion(r2)
        }

        val x = Random.nextInt(room.x.toInt(), (room.x + room.z).toInt() + 1)
        val y = Random.nextInt(room.y.toInt(), (room.y + room.w).toInt() + 1)

        builder.addEntity(item, Position3D.create(x, y, 0))

        return r2
    }
}
