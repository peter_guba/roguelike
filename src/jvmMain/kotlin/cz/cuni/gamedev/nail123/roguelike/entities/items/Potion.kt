package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Tile
import kotlin.math.min

class Potion(val healingFactor: Int = 5): Item(GameTiles.POTION) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        if (character is Player && character.hitpoints == character.maxHitpoints) {
            return Inventory.EquipResult(false, "Your HP is already full")
        }

        return Inventory.EquipResult.Success
    }

    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.hitpoints += healingFactor
            character.hitpoints = min(character.hitpoints, character.maxHitpoints)
        }

        character.inventory.remove(this)
    }

    override fun onUnequip(character: HasInventory) {}

    override fun toString(): String {
        return "Healing Potion($healingFactor)"
    }
}