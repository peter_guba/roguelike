package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.MovingEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.entities.items.*
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import org.hexworks.zircon.api.data.Tile
import kotlin.random.Random
import kotlin.math.min

abstract class Enemy(tile: Tile, val level: Int): MovingEntity(tile), HasCombatStats, Interactable, Interacting {
    override val blocksMovement = true

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) { player -> Combat.attack(player, this@Enemy) }
    }

    override fun interactWith(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) { player -> Combat.attack(this@Enemy, player) }
    }

    override fun die() {
        super.die()
        // Drop an item

        val r1 = Random.nextInt(0, 3)
        val r2 = min(20, Random.nextInt(1, level * 2 + 1))

        var item: Item
        if (r1 == 0) {
            item = Sword(r2)
        }
        else if (r1 == 1) {
            item = MetalArmor(r2)
        }
        else {
            item = Potion(r2)
        }

        this.block.entities.add(item)
    }
}