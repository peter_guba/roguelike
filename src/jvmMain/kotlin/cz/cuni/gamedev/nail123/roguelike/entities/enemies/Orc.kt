package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Orc(
    override val smellingRadius: Int = 14,
    override var hitpoints: Int = 12,
    override var attack: Int = 6,
    override var defense: Int = 0,
    level: Int = 5
): Enemy(GameTiles.ORC, level), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false

    override val maxHitpoints = 20

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }
}
