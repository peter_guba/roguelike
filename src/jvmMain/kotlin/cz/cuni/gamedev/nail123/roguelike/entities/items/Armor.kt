package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import org.hexworks.zircon.api.data.Tile

abstract class Armor(tile: Tile): Item(tile) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return if (character.inventory.equipped.filterIsInstance<Armor>().isNotEmpty()) {
            Inventory.EquipResult(false, "Cannot equip two sets of armor")
        } else Inventory.EquipResult.Success
    }
}